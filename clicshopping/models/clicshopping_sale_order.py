# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _

class sale_order(osv.osv):
    _inherit = "sale.order"
    _description = "Order Sales ClicShopping"

    _columns = {  
#order
		'clicshopping_order_id': fields.integer('Order Id', size=10, help="Id of the order"),
		'clicshopping_order_reference': fields.char('Order Reference', size=10, help="Reference of order save in ClicShopping"),
		'clicshopping_order_date_purchased': fields.char('Date Purchase', size=10, help="Date of order purchasing"),

		'clicshopping_order_status': fields.selection([('instance','Instance'),
                                                   ('processing','Processing'),
                                                   ('delivered','Delivered'),
                                                   ('cancelled','Cancelled')],'Status'),

		'clicshopping_order_orders_archive': fields.boolean('Order archive', size=20, help="Archive the order"),
#customer
		'clicshopping_order_customer_id': fields.integer('Customer Id', size=10, help="Id of the customer"),
		'clicshopping_order_customers_group_id': fields.integer('Customer Group', size=20, help="Customer Group"),
		'clicshopping_order_customers_company': fields.char('Customer Company name', size=20),
		'clicshopping_order_customers_ape': fields.char('Ape Number', size=20),
		'clicshopping_order_customers_siret': fields.char('Siret Number', size=20),
		'clicshopping_order_customers_tva_intracom': fields.char('Intracom number', size=20),
		'clicshopping_order_customer_comments': fields.text('Customer comments', help="Comments about this order"),
#localisation
        'clicshopping_order_client_computer_ip': fields.char('Customer computer IP', size=10, help="Ip of customer"),
		'clicshopping_order_provider_name_client': fields.char('Customer provider Name', size=20, help="Provider of customer"),

#payment
		'clicshopping_order_payment_method': fields.char('Payment method', size=20,),
		'clicshopping_order_cc_type': fields.char('CC Type', size=20, help="Information credit card number"),
		'clicshopping_order_cc_owner': fields.char('CC Owner', size=20, help="Information credit card number"),
		'clicshopping_order_cc_number': fields.char('CC Number', size=20, help="Information credit card number"),
		'clicshopping_order_cc_expires': fields.char('CC Expires', size=20, help="Information credit card number"),
    }
