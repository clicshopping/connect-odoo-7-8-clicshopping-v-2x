from openerp.osv import fields, osv
from openerp.tools.translate import _

class sale_order(osv.osv):
    _inherit = "account.invoice"
    _description = "Invoice ClicShopping"

    _columns = {  
#order
		'clicshopping_invoice_id': fields.integer('Order Id', size=10, help="Id of the order"),
		'clicshopping_invoice_reference': fields.char('Order Reference', size=10, help="Reference of order save in ClicShopping"),
		'clicshopping_invoice_date_purchased': fields.char('Date Purchase', size=10, help="Date of order purchasing"),

		'clicshopping_invoice_status': fields.selection([('instance','Instance'),
                                                     ('processing','Processing'),
                                                     ('delivered','Delivered'),
                                                     ('cancelled','Cancelled')],'Status'),

		'clicshopping_invoice_orders_archive': fields.boolean('Order archive', size=20, help="Archive the order"),
#customer
		'clicshopping_invoice_customer_id': fields.integer('Customer Id', size=10, help="Id of the customer"),
		'clicshopping_invoice_customers_group_id': fields.integer('Customer Group', size=20, help="Customer Group"),
		'clicshopping_invoice_customers_company': fields.char('Customer Company name', size=20),
		'clicshopping_invoice_customers_ape': fields.char('Ape Number', size=20),
		'clicshopping_invoice_customers_siret': fields.char('Siret Number', size=20),
		'clicshopping_invoice_customers_tva_intracom': fields.char('Intracom number', size=20),
		'clicshopping_invoice_customer_comments': fields.text('Customer comments', help="Comments about this order"),
#localisation
        'clicshopping_invoice_client_computer_ip': fields.char('Customer computer IP', size=10, help="Ip of the customer"),
		'clicshopping_invoice_provider_name_client': fields.char('Customer provider Name', size=20, help="provider of the customer"),

#payment
		'clicshopping_invoice_payment_method': fields.char('Payment method', size=20,),
		'clicshopping_invoice_cc_type': fields.char('CC Type', size=20, help="Information credit card number"),
		'clicshopping_invoice_cc_owner': fields.char('CC Owner', size=20, help="Information credit card number"),
		'clicshopping_invoice_cc_number': fields.char('CC Number', size=20, help="Information credit card number"),
		'clicshopping_invoice_cc_expires': fields.char('CC Expires', size=20, help="Information credit card number"),
    }
